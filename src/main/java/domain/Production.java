package domain;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
public class Production {
    String productionId;
    int wattage;
    LocalDateTime time;
}
