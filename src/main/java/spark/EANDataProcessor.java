package spark;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

public class EANDataProcessor {
    private static final Logger LOGGER = Logger.getLogger(EANDataProcessor.class);

    public static void main(String[] args) {
        SparkConf sparkConf = new SparkConf().setAppName("EAN Dataprocessor");
        if (!sparkConf.contains("spark.master")) {
            sparkConf.setMaster("local[*]");
            LOGGER.log(Level.WARN, "No spark.master detected: Running in local mode");
        }
    }

    SparkConf sparkConf = new SparkConf().setAppName("EAN Dataprocessor");




    JavaStreamingContext ssc = new JavaStreamingContext(sparkConf, Durations.seconds(1));

    //JavaPairRDD<String, Integer> initialRDD = ssc.sparkContext().parallelizePairs(tuples);
}
