package spark;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import scala.collection.JavaConversions;

import static java.util.Arrays.asList;


public class EANRunnerFunctions {
    private static final Logger LOGGER = Logger.getLogger(EANRunnerFunctions.class);

    SparkConf conf;
    SparkContext sc;

    public EANRunnerFunctions() {
        conf = new SparkConf().setAppName("EAN Procesor");
        if (!conf.contains("spark.master")) {
            conf.setMaster("local[*]");
            LOGGER.log(Level.WARN, "No spark.master detected: Running in local mode");
        }
        sc = new SparkContext(conf);
    }

    public void readTotals(String basePath) {
        SparkSession spark = SparkSession.builder().sparkContext(sc).getOrCreate();

        StructType userSchema = new StructType().add("id", "string").add("wattage", "integer").add("time", "Timestamp");
        Dataset<Row> measurements = spark.read().schema(userSchema).option("timestampFormat","yyyy-MM-dd HH:mm").json(basePath + "1.json");
        Dataset<Row> eans = spark.read().json(basePath + "eans.json");

        //join tables
        Dataset<Row> eanMeasurements = measurements.toDF().join(eans, "id");
        eanMeasurements.show();

        //Cutting date from field
        eanMeasurements = eanMeasurements.withColumn("time", functions.concat(
                functions.split(eanMeasurements.col("time"), " ").getItem(1)
        ));
        //Cutting seconds from field
        eanMeasurements = eanMeasurements.withColumn("time", functions.concat(
                functions.split(eanMeasurements.col("time"), ":").getItem(0),
                functions.lit(":"),
                functions.split(eanMeasurements.col("time"), ":").getItem(1)
        ));

        Dataset<Row> groupedEanMeasurements = eanMeasurements.withWatermark("time", "3 minutes").groupBy("time", "postalCode","type").sum("wattage");
        groupedEanMeasurements = groupedEanMeasurements.withColumnRenamed("sum(wattage)", "totalWattage");

        /*
        groupedEanMeasurements.show();

        |21:30|      2000|CONSUMER|        4444|
        |21:30|      1000|PRODUCER|        4708|
        |21:29|      2000|CONSUMER|        7820|
        |21:30|      3000|PRODUCER|        5336|
        |21:31|      1000|CONSUMER|        4552|
        |21:28|      2000|PRODUCER|        3192|
        |21:31|      2000|CONSUMER|        6388|
        |21:29|      2000|PRODUCER|        8468|
        |21:30|      2000|PRODUCER|        5968|
        |21:28|      3000|CONSUMER|        2240|
        |21:29|      1000|PRODUCER|        5920|
        |21:28|      3000|PRODUCER|        1808|
        |21:30|      3000|CONSUMER|        6744|
         */
        Dataset<Row> eanConsumerMeasurements = groupedEanMeasurements.filter(eanMeasurements.col("type").equalTo("CONSUMER"));
        eanConsumerMeasurements = eanConsumerMeasurements.withColumnRenamed("totalWattage", "consumption");
        eanConsumerMeasurements.show();
        //
        Dataset<Row> eanProducerMeasurements = groupedEanMeasurements.filter(eanMeasurements.col("type").equalTo("PRODUCER"));
        eanProducerMeasurements = eanProducerMeasurements.withColumnRenamed("totalWattage", "production");
        eanProducerMeasurements.show();
        //
        Dataset<Row> allEanMeasurements = eanConsumerMeasurements.toDF().join(eanProducerMeasurements, JavaConversions.asScalaBuffer(asList("time","postalCode")));
        allEanMeasurements = allEanMeasurements.drop("type");


        allEanMeasurements.show();
        /*
        +-----+----------+-----------+----------+
        | time|postalCode|consumption|production|
        +-----+----------+-----------+----------+
        |21:44|      2000|       6532|      7424|
        |21:48|      1000|       8404|      3816|
        |21:31|      1000|       6468|      4688|
        |22:04|      2000|       5848|      7484|
        |22:15|      2000|       7124|      4976|
        |22:15|      3000|       6888|      4504|
        |22:09|      2000|       6316|      7424|
        |22:01|      2000|       6360|      6416|
        |21:37|      2000|       7656|      4704|
        |22:02|      2000|       7400|      7228|
        |21:34|      2000|       6016|      5176|
        |21:35|      2000|       8600|      5644|
        |22:16|      1000|       8724|      1288|
        |21:33|      2000|       7436|      9864|
        |21:57|      3000|       4300|      3936|
        |21:31|      2000|       8656|      8648|
        |21:33|      1000|       9600|      4600|
        |21:45|      2000|       9620|      7140|
        |22:11|      2000|       4620|      8052|
        |21:34|      1000|       6216|      4444|
        +-----+----------+-----------+----------+
         */

        //allEanMeasurements.write().mode("append").parquet(basePath + "test.parquet");
    }
}
